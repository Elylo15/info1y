A day in the life
A little help from my friends
A taste of honey
Across the universe
Act naturally
All I've got to do
All my loving
All you need is love
And I love her
And your bird can sing
Anna (go to him)
Another girl
Any time at all
Ask me why
Baby it's you
Baby's in black
Baby, you're a rich man
Back in the U.S.S.R.
Bad boy
Being for the benefit of Mr. Kite!
Birthday
Blackbird
Blue jay way
Boys
Can't buy me love
Carry that weight
Chains
Come together
Cry baby cry
Day tripper
Dear prudence
Devil in her heart
Dig a pony
Dizzy Miss Lizzie
Do you want to know a secret
Doctor Robert
Don't bother me
Don't let me down
Don't pass me by
Drive my car
Eight days a week
Eleanor Rigby
Every little thing
Fixing a hole
For no one
For you blue
Free as a bird
From me to you
Get back
Getting better
Girl
Glass onion
Golden slumbers
Good day sunshine
Good morning, good morning
Good night
Got to get you into my life
Happiness is a warm gun
Hello, goodbye
Help!
Helter skelter
Here comes the sun
Here, there, and everywhere
Hey Jude
Hey bulldog
Hold me tight
Honey don't
Honey pie
I am the Walrus
I don't want to spoil the party
I feel fine
I me mine
I need you
I saw her standing there
I wanna be your man
I want to hold your hand
I want to tell you
I will
I'll be back
I'll cry instead
I'll follow the sun
I'll get you
I'm a loser
I'm down
I'm just happy to dance with you
I'm looking through you
I'm only sleeping
I'm so tired
I've got a feeling
I've just seen a face
If I fell
If I needed someone
In my life
It won't be long
It's all too much
It's only love
Julia
Lady Madonna
Let it be
Little child
Long tall Sally
Long, long, long
Love me do
Love you to
Lovely Rita
Lucy in the Sky with Diamonds
Magical mystery tour
Martha my dear
Matchbox
Maxwell's silver hammer
Mean Mr. Mustard
Michelle
Misery
Money (that's what I want)
No reply
Norwegian wood
Not a second time
Nowhere man
Ob-la-di, ob-la-da
Octopus's garden
Oh! Darling
Old brown shoe
One after 909
Only a northern song
P.S. I love you
Paperback writer
Penny Lane
Piggies
Please mister postman
Please please me
Rain
Real love
Revolution
Rock and roll musicRocky raccoon
Roll over Beethoven
Run for your life
Savoy truffle
Sexy Sadie
Sgt. Pepper's Lonely Hearts Club Band
She came in through the bathroom window
She loves you
She said, she said
She's a woman
She's leaving home
Slow down
Something
Strawberry fields forever
Taxman
Tell me what you see
Tell me why
Thank you girl
The ballad of John and Yoko
The continuing story of Bungalow Bill
The fool on the hill
The inner light
The long and winding road 
The night before
The word
There is a place,
Things we said today
Think for yourself
This boy
Ticket to ride
Till there was you
Tomorrow never knows
Twist and shout
Two of us
We can work it out
Wait
What goes on
What you're doing
When I get home
When I'm sixty-four
While my guitar gently weeps
Within you without you
Yellow submarine
Yer blues
Yes it is
Yesterday
You can't do that
You know my name (look up the number)
You like me too much
You never give me your money You never give me your money
You really got a hold on me
You won't see me
You're going to lose that girl
You've got to hide your love away
