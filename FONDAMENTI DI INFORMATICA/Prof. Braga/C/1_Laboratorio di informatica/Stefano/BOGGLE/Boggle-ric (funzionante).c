#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

typedef struct{
	char boggle[4][4];
} structg;

int verifica(structg griglia, char parola2[], int i, int t);
int contenuta(structg griglia, char parola2[]); //verifica se la parola � contenuta nel boggle (senza ritorni sulla stessa casella, quindi versione classica)
int contrl(char str[], FILE * file); //controlla se la parola esiste nel dizionario Italiano
void inizializza(structg *griglia); //inizializza il boggle a tutti '\0'
void inserisci(structg *griglia, char str[17]);	//inserisci una stringa nel boggle
void stampa(structg *griglia);	//stampa il boggle

int main(){
	
	int d=0;
	FILE * file;
	char parola[17]="";
	char str[17] = "MNAPSIPECUELCHNA";
	structg griglia;
	int risposte[17];
	inizializzarray(risposte, 17);
	structg *p;
	p=&griglia;
	inizializza(p);
	inserisci(p, str);
	printf("---Boggle---\n");
	stampa(p);	//stampa delLa griglia
	//inizio seconda parte
	printf("Inserisci il maggior numero di parole che riesci a riconoscere\nScrivi una parola (o scrivi \"fine\" per terminare): ");
	scanf("%s", parola);
	for(; (strcmp(parola, "fine"))!=0 ;){
		if(contrl(parola, file)==-1){
			printf("Errore nell'apertura' del file \"dizionarioitaliano.txt\"");
			return 0;
		}
		if(contenuta(griglia, parola) && contrl(parola, file)){
			printf("Hai trovato una parola!\n");
			risposte[strlen(parola)]+=1;
			d++;
		}
		else if(contenuta(griglia, parola) && (!contrl(parola, file))){
			printf("La parola c'e' ma non esiste (nel vocabolario)\n");
		}
		else if(!contenuta(griglia, parola)){
			printf("La parola non e' contenuta nella griglia\n");
		}
		stampa(&griglia);
		printf("Scrivi un'altra parola (o scrivi \"fine\" per terminare): ");
		scanf("%s", parola);
	}
	printf("Hai trovato %d parole, e totalizzato %d punti", d, contapunti(risposte));
	
	
	
	return 0;
}

int contapunti(int * risposte){
	int n, punti=0;
	for( n=1 ; n<=16 ; n++ ){
		punti+=risposte[n]*n;
	}
	return punti;
}

void inizializzarray(int * array, int n){
	for( n-=1 ; n>=0 ; n--){
		array[n]=0;
	}
}

int contenuta(structg griglia, char parola2[]){
	int i, t;
	char parola[17]="";
	for(i=0; i<=strlen(parola2)-1; i++){
		if(parola2[i]>='a' && parola2[i]<='z'){
			parola[i]=(parola2[i]-('a'-'A'));
		}
		else{
			parola[i]=parola2[i];
		}
	}
	parola[i]='\0';
	for(i=0; i<4; i++){
		for(t=0; t<4; t++){
			if(parola[0]==griglia.boggle[i][t]){
				if(verifica(griglia, parola+1, i, t)){
					return 1;
				}
			}
		}
	}
	return 0;
}

int verifica(structg griglia, char parola[], int i, int t){
	structg griglia2;
	griglia2=griglia;
	int k=0;
	griglia.boggle[i][t]='\0';
	if((strlen(parola))==0){
		return 1;
	}
	if((i-1>=0) && (t-1>=0) && parola[k]==griglia.boggle[i-1][t-1]){ //verifica con "ricordo" (con la tecnica del '\0') (per tutti gli if)
		griglia=griglia2;
		griglia.boggle[i-1][t-1]='\0';
		if(verifica(griglia, parola+1, i-1, t-1)){
			return 1;
		}
	}
	if((i-1>=0) && parola[k]==griglia.boggle[i-1][t]){
		griglia=griglia2;
		griglia.boggle[i-1][t]='\0';
		if(verifica(griglia, parola+1, i-1, t)){
			return 1;
		}
	}
	if((i-1>=0) && (t+1<=3) && parola[k]==griglia.boggle[i-1][t+1]){
		griglia=griglia2;
		griglia.boggle[i-1][t+1]='\0';
		if(verifica(griglia, parola+1, i-1, t+1)){
			return 1;
		}
	}
	if((t-1>=0) && parola[k]==griglia.boggle[i][t-1]){
		griglia=griglia2;
		griglia.boggle[i][t-1]='\0';
		if(verifica(griglia, parola+1, i, t-1)){
			return 1;
		}
	}
	if((t+1<=3) && parola[k]==griglia.boggle[i][t+1]){
		griglia=griglia2;
		griglia.boggle[i][t+1]='\0';
		if(verifica(griglia, parola+1, i, t+1)){
			return 1;
		}
	}
	if((i+1<=3) && (t-1>=0) && parola[k]==griglia.boggle[i+1][t-1]){
		griglia=griglia2;
		griglia.boggle[i+1][t-1]='\0';
		if(verifica(griglia, parola+1, i+1, t-1)){
			return 1;
		}
	}
	if((i+1<=3) && parola[k]==griglia.boggle[i+1][t]){
		griglia=griglia2;
		griglia.boggle[i+1][t]='\0';
		if(verifica(griglia, parola+1, i+1, t)){
			return 1;
		}
	}
	if((i+1<=3) && (t+1<=3) && parola[k]==griglia.boggle[i+1][t+1]){
		griglia=griglia2;
		griglia.boggle[i+1][t+1]='\0';
		if(verifica(griglia, parola+1, i+1, t+1)){
			return 1;
		}
	}
	return 0;
}

int contrl(char str[], FILE * file){ //controlla se la parola esiste nel dizionario Italiano
						//e restituisce -1 se ci sono errori nell'apertura del file txt
	char vett[20]="";
	if((file=fopen("italiandictionary.txt", "r"))!=NULL){
		for(;! feof(file);){
			fgets(vett, 90, file);
			vett[strlen(vett)-1]='\0';
			if(strcmp(vett, str)==0){
				fclose(file);
				return 1;
			}
		}
		fclose(file);
		return 0;
	}
	else{
		return -1;
	}
}

void inserisci(structg *griglia, char str[17]){
	int i, t, k;
	for(k=0; k<16;){
		for(i=0; i<4; i++){
			for(t=0; t<4; t++){
				(*griglia).boggle[i][t]=str[k];
				k++;
			}
		}
	}
}

void inizializza(structg *griglia){
	int i, t;
	for(i=0; i<4; i++){
		for(t=0; t<4; t++){
			(*griglia).boggle[i][t]='\0';
		}
	}
}

void stampa(structg *griglia){
	int i;
	for(i=0; i<4; i++){
		printf("-----------------\n");
		printf("| %c | %c | %c | %c |\n", (*griglia).boggle[i][0], (*griglia).boggle[i][1], (*griglia).boggle[i][2], (*griglia).boggle[i][3]);
	}
	printf("-----------------\n");
}
