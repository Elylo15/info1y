/*
Scrivere un programma che, dati in input due interi a e b non negativi, stampi a video tutti i numeri Armstrong compresi tra e b.

NOTA: un numero di 3 cifre è di Armstrong se la somma dei cubi delle sue cifre è uguale al numero stesso.
Esempio: 371 è di Armstrong perchè:
        3^3 = 27
        7^3 = 343
        1^3 = 1
        e 27 +343 + 1 = 371
*/

#include <stdio.h>
#include <math.h>

int is_armstrong(int num);

int main()
{
    int a,b,i,armstrong,temp,tmp,almenoUno = 0;

    do {
        printf("Inserisci il primo valore positivo: ");
        scanf("%d%*c", &a);
        printf("Inserisci il secondo valore positivo < 1000: ");
        scanf("%d%*c", &b);
        if(a > b) { //voglio che a sia minore di b. Se l'utente inserisce a>b, inverto a con b
            tmp = a;
            a = b;
            b = tmp;
        }
    } while (a < 0 || b > 999); //continuo a chiedere i numeri se l'utente mette a < 0 o b > 999


    for(i=a; i<=b; i++) { //cicla per i (numeri) che partono da a e sono minori di b
        if(is_armstrong(i)) {
            printf("Il numero %d è numero di Armstrong\n", i);
            almenoUno = 1;
        }
    }

    if(almenoUno == 0) {
        printf("Nessun numero di Armstrong trovato nell'intervallo definito");
    }

    return 0;
}

int is_armstrong(int num) {
    int temp = num, armstrong = 0;
    while(temp) { //questo while serve per dividere il numero di n cifre nelle singole cifre. Poi eleva ogni cifra alla terza
        armstrong = armstrong + pow(temp % 10, 3);
        temp = temp / 10;
    }

    return armstrong == num; //ritorna 1 se è vero, 0 se è falso

}
