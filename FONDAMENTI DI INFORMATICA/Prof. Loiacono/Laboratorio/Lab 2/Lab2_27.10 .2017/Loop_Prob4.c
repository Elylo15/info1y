#include <stdio.h>

/*
    FUNZIONAMENTO: salvo in n0 il valore precedente, in n il valore attuale e in n1 quello successivo. Comparo n con n0 e n1 per vedere se è max o min o nessuno dei due
*/

int leggiNumeroInteroPositivo();

int main()
{
    int i=1, n0, n, n1, max_locali = 0, min_locali = 0;

    printf("Inserisci una sequenza di numeri interi positivi (-1 per terminare)\n");

    do {
        if(i==1) { //inserisco il primo valore ---> non deve ancora cercare max e min
            n = leggiNumeroInteroPositivo();
            n0 = n;
        }
        else if(i==2) { //inserisco il secondo valore ---> non deve ancora cercare max e min
            n = leggiNumeroInteroPositivo();
        }
        else if(i>2) { //inserisco il terzo valore ---> ora può iniziare a cercare i max e min locali
            n1 = leggiNumeroInteroPositivo();
            if(n1 != -1) { //se n = -1 devo saltare questo blocco, altrimenti utilizza anche il -1 nella sequenza
                if(n>n0 && n>n1) {
                    max_locali++;
                    printf("%d --- %d (MAX) --- %d\n", n0, n, n1);
                } else if (n<n0 && n<n1) {
                    printf("%d --- %d (MIN) --- %d\n", n0, n, n1);
                    min_locali++;
                } else
                    printf("%d --- %d --- %d\n", n0, n, n1);

            }
            n0 = n; //preparo le variabili per la comparazione alla ciclazione successiva
            n = n1;
        }
        i++;
    } while(n != -1); //utilizzo -1 per stoppare la sequenza

    printf("\nNella successione inserita sono presenti:\n<> %d MAX locali\n<> %d MIN locali", max_locali, min_locali);

    return 0;
}

int leggiNumeroInteroPositivo() {

    float num_orig;
    int num=0;

    do {
        //printf("Inserisci un numero intero positivo\n");
        scanf("%f%*c", &num_orig);
        num = num_orig;
    } while((num < 1 && num > -1 || num < -1) || num != num_orig); //cicla finchè non inseriamo un numero positivo e intero, ma acceta il -1

    return num;
}
